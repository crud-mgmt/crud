package com.ndb.crud.model;

import com.ndb.crud.annotation.Column;
import com.ndb.crud.common.constants.QueryOptionEnum;
import com.ndb.crud.common.model.CommonQueryReq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.*;

public class CommonQuery {
    private Logger logger= LoggerFactory.getLogger(this.getClass());

    //key-tabName,value-fields
    private Map<String,Map<String,String>> tabFieldsMap=new HashMap<>();

    private List<ConditionFields> fields=new ArrayList<>();

    private List<String> orderList;

    private static List<String> orderEum=Arrays.asList("asc","desc");
    /**排序方式 asc,desc*/
    private String orderCase="asc";


    private Class<?> cls;

    private int pageNum;
    private int pageSize;

    public CommonQuery(Class<?> cls, CommonQueryReq req) {
        this.cls=cls;
        this.pageNum=req.getPageNum();
        this.pageSize=req.getPageSize();
        initObjFields(cls);
        initCommonQueryReq(req);
        setOrderCase(req.getOrderCase());
    }

    public CommonQuery(Class<?> cls) {
        this.cls=cls;
        initObjFields(cls);
    }

    public <TModel> CommonQuery(TModel model) {
        this.cls=model.getClass();
        initObjFields(cls);
        initCommonQueryModel(model);
    }

    private <TModel> void initCommonQueryModel(TModel model) {
        Field[] fields = this.cls.getDeclaredFields();
        Arrays.stream(fields).forEach(field->{
            field.setAccessible(true);
            try {
                Object value = field.get(model);
                if(value!=null){
                    addFieldEq(field.getName(),(String) value);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }

    private void initCommonQueryReq(CommonQueryReq req) {
        List<ConditionFields> fields = req.getFields();
        if(!CollectionUtils.isEmpty(fields)){
            fields.forEach(this::addField);
        }

        List<String> orderList = req.getOrderList();
        if(!CollectionUtils.isEmpty(orderList)){
            orderList.forEach(this::addFieldOrder);
        }
    }

    /**
     * 将属性字段替换为数据库表字段
     * @param field
     */
    private void addFieldOrder(String field) {
        String tabName = this.cls.getName();
        Map<String, String> tabFields = tabFieldsMap.get(tabName);
        String dbField = tabFields.get(field);
        if(StringUtils.isEmpty(dbField)){
            logger.warn("属性："+field+"在对象:"+tabName+"不存在");
            return;
        }
        this.orderList.add(dbField);
    }

    /**
     * 将属性字段替换为数据库表字段
     * @param field
     */
    private void addField(ConditionFields field) {
        String tabName = this.cls.getName();
        Map<String, String> tabFields = tabFieldsMap.get(tabName);
        String fieldName = field.getField();
        String dbField = tabFields.get(fieldName);
        if(StringUtils.isEmpty(dbField)){
            logger.warn("属性："+fieldName+"在对象:"+tabName+"不存在");
            return;
        }
        field.setField(dbField);
        fields.add(field);
    }

    /**
     * 扫描类是否有Column注解，封装到map中
     * @param cls
     */
    private void initObjFields(Class<?> cls) {
        String tabName = cls.getName();
        Map<String, String> tabFields = this.tabFieldsMap.get(tabName);
        if(!CollectionUtils.isEmpty(tabFields)){
            return;
        }
        tabFields=new HashMap<>();
        Field[] fields = cls.getDeclaredFields();

        for (Field field : fields){
            boolean isColumn = field.isAnnotationPresent(Column.class);
            if(isColumn){
                Column annotation = field.getAnnotation(Column.class);
                //获取注解值
                String dbFieldName = annotation.dbFieldName();
                if(StringUtils.isEmpty(dbFieldName)){
                    dbFieldName = field2DbField(field.getName());
                }
                tabFields.put(field.getName(),dbFieldName);
            }else{
                String dbFieldName = field2DbField(field.getName());
                tabFields.put(field.getName(),dbFieldName);
            }
        }
        this.tabFieldsMap.put(tabName,tabFields);
    }

    /**
     * 新增查询条件字段，验证字段是否包含类中
     * @param key
     * @param value
     * @return
     */
    public <T> CommonQuery addFieldEq(String key, T value){
        ConditionFields field=new ConditionFields(key,value, QueryOptionEnum.EQ.getName());
        addField(field);
        return this;
    }

    public <T> CommonQuery addFieldNotEq(String key, T value){
        ConditionFields field=new ConditionFields(key,value, QueryOptionEnum.NOT_EQ.getName());
        addField(field);
        return this;
    }

    public <T> CommonQuery addFieldLike(String key, T value){
        ConditionFields field=new ConditionFields(key,value,QueryOptionEnum.LIKE.getName());
        addField(field);
        return this;
    }

    public CommonQuery addFieldIn(String key,List<?> value){
        ConditionFields field=new ConditionFields(key,value,QueryOptionEnum.IN.getName());
        addField(field);
        return this;
    }

    /**
     * 大于
     * @param key
     * @param value
     * @return
     */
    public <T> CommonQuery addFieldGt(String key, T value){
        ConditionFields field=new ConditionFields(key,value,QueryOptionEnum.GT.getName());
        addField(field);
        return this;
    }

    /**
     * 大于等于
     * @param key
     * @param value
     * @return
     */
    public <T> CommonQuery addFieldGtEq(String key, T value){
        ConditionFields field=new ConditionFields(key,value,QueryOptionEnum.GT_EQ.getName());
        addField(field);
        return this;
    }

    /**
     * 小于
     * @param key
     * @param value
     * @return
     */
    public <T> CommonQuery addFieldLt(String key, T value){
        ConditionFields field=new ConditionFields(key,value,QueryOptionEnum.LT.getName());
        addField(field);
        return this;
    }

    /**
     * 小于等于
     * @param key
     * @param value
     * @return
     */
    public <T> CommonQuery addFieldLtEq(String key, T value){
        ConditionFields field=new ConditionFields(key,value,QueryOptionEnum.LT_EQ.getName());
        addField(field);
        return this;
    }

    public <T> CommonQuery addFieldBetween(String key, T startVal, T endVal){
        ConditionFields field=new ConditionFields(key,startVal,endVal,QueryOptionEnum.BETWEEN.getName());
        addField(field);
        return this;
    }

    /**
     * 字段转换成数据库字段：
     * 如：fieldName转换为field_name
     * @param fieldName
     * @return
     */
    public static String field2DbField(String fieldName){
        if(StringUtils.isEmpty(fieldName)){
            return null;
        }
        char[] chars = fieldName.toCharArray();
        List<Character> fields=new ArrayList<>();
        for (char ch:chars){
            //大写字符
            if(ch>=65&&ch<=90){
                fields.add('_');
                ch= (char) (ch+32);
            }
            fields.add(ch);
        }
        StringBuilder sb=new StringBuilder();
        fields.forEach(item->sb.append(item));
        return sb.toString();
    }

    public List<ConditionFields> getFields(){
        return this.fields;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderCase() {
        return orderCase;
    }

    public void setOrderCase(String orderCase) {
        if(orderEum.contains(orderCase)){
            this.orderCase = orderCase;
        }
        logger.warn("sql注入警告：非法的排序值："+orderCase);
    }
}
