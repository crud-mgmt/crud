package com.ndb.crud.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 字段排序
 */
@Getter
@Setter
public class OrderModel {
    /**
     * 排序字段
     */
    private String field;

    /**
     * 排序方式：desc，asc
     */
    private String order;
}
