package com.ndb.crud.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConditionFields {
    private String field;

    private Object value;

    private Object value2;
    /***
     * 操作，大于小于，等于，like
     */
    private String option;

    public ConditionFields(String field, Object value, String option) {
        this.field = field;
        this.value = value;
        this.option = option;
    }

    public ConditionFields(String field, Object startVal,Object endVal, String option) {
        this.field = field;
        this.value = startVal;
        this.value2=endVal;
        this.option = option;
    }
}
