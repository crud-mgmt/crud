package com.ndb.crud.model.form;

import java.io.Serializable;
import java.util.List;

public class Column extends Field implements Serializable {
    private int width;
    /**
     * 表格列的类型：action-操作列，text-文本，img-图片
     */
    private String tbType;

    private List<Action> actions;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getTbType() {
        return tbType;
    }

    public void setTbType(String tbType) {
        this.tbType = tbType;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }
}
