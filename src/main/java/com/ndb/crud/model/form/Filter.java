package com.ndb.crud.model.form;

import java.io.Serializable;

public class Filter extends Field implements Serializable {
    /**
     * 字段名称
     */
    private String id;
    /**
     * 查询类型eq,like,between
     */
    private String option;

    public Filter(String id, String option) {
        this.id = id;
        this.option = option;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}
