package com.ndb.crud.model.form;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class BaseForm implements Serializable {
    private String id;

    private String name;

    /**
     * 基础的字段定义
     */
    private Map<String,Field> fields;
    /**
     * 查询条件过滤
     */
    private List<Filter> filters;
    /**
     * 表格排序
     */
    private Orders orders;

    /**
     * 页面操作按钮
     */
    private List<Action> actions;
    /**
     * 新增表单
     */
    private ShowForm createForm;
    /**
     * 更新表单
     */
    private ShowForm updateForm;
    /**
     * 详情表单
     */
    private ShowForm detailForm;
    /**
     *通用新增/更新/详情表单
     */
    private ShowForm crudForm;
    /**
     * 公共通用表单
     */
    private Map<String,ShowForm> commonForms;

    /**
     * 表格展示的列
     */
    private List<Column> columns;

    private Map<String,List<OptionRef>> optionRefs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Field> getFields() {
        return fields;
    }

    public void setFields(Map<String, Field> fields) {
        this.fields = fields;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public ShowForm getCreateForm() {
        return createForm;
    }

    public void setCreateForm(ShowForm createForm) {
        this.createForm = createForm;
    }

    public ShowForm getUpdateForm() {
        return updateForm;
    }

    public void setUpdateForm(ShowForm updateForm) {
        this.updateForm = updateForm;
    }

    public ShowForm getDetailForm() {
        return detailForm;
    }

    public void setDetailForm(ShowForm detailForm) {
        this.detailForm = detailForm;
    }

    public ShowForm getCrudForm() {
        return crudForm;
    }

    public void setCrudForm(ShowForm crudForm) {
        this.crudForm = crudForm;
    }

    public Map<String, ShowForm> getCommonForms() {
        return commonForms;
    }

    public void setCommonForms(Map<String, ShowForm> commonForms) {
        this.commonForms = commonForms;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public Map<String, List<OptionRef>> getOptionRefs() {
        return optionRefs;
    }

    public void setOptionRefs(Map<String, List<OptionRef>> optionRefs) {
        this.optionRefs = optionRefs;
    }
}
