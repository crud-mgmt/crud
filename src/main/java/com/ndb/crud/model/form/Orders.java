package com.ndb.crud.model.form;

import java.io.Serializable;
import java.util.List;

public class Orders implements Serializable {
    /**
     * 列表排序
     * asc,desc
     */
    private String sort;

    private List<String> orders;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public List<String> getOrders() {
        return orders;
    }

    public void setOrders(List<String> orders) {
        this.orders = orders;
    }
}
