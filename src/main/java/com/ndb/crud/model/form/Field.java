package com.ndb.crud.model.form;

import java.io.Serializable;
import java.util.List;

public class Field implements Serializable {
    /**
     * 字段属性
     */
    private String id;
    /**
     * 字段类型：select,input,radio
     */
    private String type;
    /**
     * 展示的字段中文
     */
    private String name;
    /**
     * 录入时的正则
     */
    private String regexp;

    /**
     * 内容格式化
     */
    private String format;

    /**
     * 字段值
     */
    private String value;

    private boolean required;

    /**
     * 下拉框枚举值
     */
    private List<OptionRef> optionRef;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegexp() {
        return regexp;
    }

    public void setRegexp(String regexp) {
        this.regexp = regexp;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public List<OptionRef> getOptionRef() {
        return optionRef;
    }

    public void setOptionRef(List<OptionRef> optionRef) {
        this.optionRef = optionRef;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
