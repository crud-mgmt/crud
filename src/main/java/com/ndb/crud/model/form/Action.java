package com.ndb.crud.model.form;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class Action implements Serializable {
    /**
     * 按钮类型，insert,update
     */
    private String id;
    private String name;
    /**
     * 跳转类型dialog-弹窗，page-跳转页面
     */
    private String acType;
    /**
     * 页面跳转时对应的路径
     */
    private String path;
    /**
     * 图标
     */
    private String icon;
    /**
     * 引用的action
     */
    private String ref;
}
