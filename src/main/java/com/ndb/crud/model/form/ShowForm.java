package com.ndb.crud.model.form;

import java.io.Serializable;
import java.util.List;

/**
 * 前段页面展示的表单
 */
public class ShowForm implements Serializable {
    private List<Row> rows;

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }
}
