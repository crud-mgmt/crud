package com.ndb.crud.model.form;

import java.io.Serializable;
import java.util.List;

public class Row implements Serializable {
    private List<Field> cols;

    public List<Field> getCols() {
        return cols;
    }

    public void setCols(List<Field> cols) {
        this.cols = cols;
    }
}
