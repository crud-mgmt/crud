package com.ndb.crud.service;

import com.ndb.crud.common.model.CommonQueryReq;
import com.ndb.crud.common.model.CrudReq;
import com.ndb.crud.common.model.PageModel;
import com.ndb.crud.common.model.ResultModel;
import com.ndb.crud.model.CrudValid;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

/**
 * 通用查询接口
 * @param <Module>
 * @param <TID>
 */
public interface CrudService<Module,TID> {

    /**
     * 分页查询
     * @param condition
     * @return
     */
    public PageModel<Module> query(CommonQueryReq condition);

    /**
     * 查询详情
     * @param req
     * @return
     */
    @Validated({CrudValid.QueryValid.class})
    public ResultModel<Module> detail(@Valid CrudReq<Module,TID> req);

    /**
     * 批量插入数据
     * @param req
     * @return
     */
    public ResultModel<Boolean> batchInsert(CrudReq<Module,TID> req);

    /**
     * 插入数据
     * @param req
     * @return
     */
    @Validated({CrudValid.UpdateValid.class})
    public ResultModel<Boolean> create(@Valid CrudReq<Module,TID> req);

    /**
     * 更新数据
     * @param req
     * @return
     */
    @Validated({CrudValid.UpdateValid.class})
    public ResultModel<Boolean> update(@Valid CrudReq<Module,TID> req);

    /**
     * 删除数据库数据
     * @param req
     * @return
     */
    @Validated({CrudValid.QueryValid.class})
    public ResultModel<Boolean> delete(@Valid CrudReq<Module,TID> req);
}
