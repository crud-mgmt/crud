package com.ndb.crud.service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ndb.crud.annotation.Column;
import com.ndb.crud.common.constants.Msg;
import com.ndb.crud.common.model.CommonQueryReq;
import com.ndb.crud.common.model.CrudReq;
import com.ndb.crud.common.model.PageModel;
import com.ndb.crud.common.model.ResultModel;
import com.ndb.crud.dao.CrudDao;
import com.ndb.crud.model.CommonQuery;
import com.ndb.crud.model.CrudValid;
import com.ndb.parent.common.exception.NdbException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;
@Validated
public abstract class BaseCurdService<Module,TID,ICrudDao extends CrudDao<Module,TID>> implements CrudService<Module,TID>{

    @Autowired
    private ICrudDao crudDao;

    @Override
    public PageModel<Module> query(@Valid CommonQueryReq condition) {
        CommonQuery commonQuery = new CommonQuery(getModelClass(),condition);
        PageHelper.startPage(condition.getPageNum(),condition.getPageSize());
        List<Module> result = crudDao.query(commonQuery);
        PageInfo<Module> pageInfo = new PageInfo<Module>(result);
        PageModel<Module> model = new PageModel<>(result);
        model.setTotal(pageInfo.getTotal());
        return model;
    }

    @Override
    @Validated({CrudValid.QueryValid.class})
    public ResultModel<Module> detail(@Valid CrudReq<Module,TID> req) {
        if(StringUtils.isEmpty(req.getKey())){
            return null;
        }
        return ResultModel.ok(crudDao.detail(req.getKey()));
    }

    @Override
    public ResultModel<Boolean> batchInsert(CrudReq<Module,TID> req) {
        return ResultModel.ok();
    }

    @Override
    @Validated({CrudValid.UpdateValid.class})
    public ResultModel<Boolean> create(@Valid CrudReq<Module,TID> req) {
        Module module = req.getData();
        module=JSON.toJavaObject((JSON) module,getModelClass());
        //validator.validate(module);
        if(checkExist(module)){
            throw new NdbException(Msg.RECORD_EXIST);
        }
        beforeProcess(module);
        crudDao.insert(module);
        return ResultModel.ok();
    }

    @Override
    @Validated({CrudValid.UpdateValid.class})
    public ResultModel<Boolean> update(@Valid CrudReq<Module,TID> req) {
        if(checkExist(req.getData())){
            throw new NdbException(Msg.RECORD_EXIST);
        }
        beforeProcess(req.getData());
        crudDao.update(req.getData(),req.getKey());
        return ResultModel.ok();
    }

    @Override
    @Validated({CrudValid.QueryValid.class})
    public ResultModel<Boolean> delete(@Valid CrudReq<Module,TID> req) {
        if(StringUtils.isEmpty(req.getKey())){
            return ResultModel.ok(false);
        }
        crudDao.delete(req.getKey());
        return ResultModel.ok();
    }

    /**
     * db数据库操作前进行的处理
     * @param model
     */
    protected void beforeProcess(Module model){

    }

    /**
     * 根据@Column主键的主键判断数据是否存在
     * @param model
     * @return
     */
    protected boolean checkExist(Module model){
        CommonQuery commonQuery=new CommonQuery(getModelClass());

        //读取注解，获取主键，设置查询条件
        Field[] fields = getModelClass().getDeclaredFields();
        for (Field field : fields){
            boolean isColumn = field.isAnnotationPresent(Column.class);
            if(!isColumn){
                continue;
            }
            Column annotation = field.getAnnotation(Column.class);
            //判断注解是否有唯一约束标志
            boolean primaryKey = annotation.identity();
            if(primaryKey){
                try {
                    field.setAccessible(true);
                    String value = (String) field.get(model);
                    //获取属性值，如果属性值不为空，则进行查询
                    if(!StringUtils.isEmpty(value)){
                        //获取自定义的数据库字段
                        String dbFieldName = annotation.dbFieldName();
                        if(StringUtils.isEmpty(dbFieldName)){
                            //默认的属性字段转数据库字段
                            dbFieldName = CommonQuery.field2DbField(field.getName());
                        }
                        commonQuery.addFieldEq(dbFieldName,value);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        if(CollectionUtils.isEmpty(commonQuery.getFields())){
            return false;
        }
        PageHelper.startPage(1,1);
        List<Module> result = crudDao.query(commonQuery);
        return !CollectionUtils.isEmpty(result);
    }

    /**
     * 获取泛型类型
     * @return
     */
    protected Class<Module> getModelClass(){
        ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
        return (Class<Module>)parameterizedType.getActualTypeArguments()[0];
    }
}
