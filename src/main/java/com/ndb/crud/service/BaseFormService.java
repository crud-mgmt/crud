package com.ndb.crud.service;
import com.alibaba.fastjson.JSONObject;
import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.Action;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.model.form.Field;
import com.ndb.crud.parse.AbstractFormParse;
import com.ndb.crud.parse.FormParse;
import com.ndb.crud.utils.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Component
public class BaseFormService  implements ApplicationContextAware {
    private static Logger logger= LoggerFactory.getLogger(BaseFormService.class);

    private static final ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();

    private static final boolean DEV_MODEL=Boolean.valueOf(System.getProperty("devModel"));

    private static final String FORM_PATH="classpath*:form/*.xml";
    private static final String I18N_PATH="classpath*:i18n/zh/*.json";
    private static final String BASE_FORM_PATH="crud";

    private Map<String, FormParse> parse=new HashMap<>();

    private Map<String,BaseForm> formMap=new HashMap<>();

    private static Map<String, JSONObject> i18Map=new HashMap<>();

    public ApplicationContext applicationContext;

    /**
     * 获取菜单数据
     * @param formName
     * @return
     */
    public BaseForm meta(String formName){
        if(DEV_MODEL){
            bootStart();
        }
        return formMap.get(formName);
    }

    private void bootStart(){
        initI18n();
        initParse();
        initForm();
    }

    private void initParse() {
        if(!CollectionUtils.isEmpty(parse)){
            return;
        }
        Map<String, FormParse> beansOfType = applicationContext.getBeansOfType(FormParse.class);
        beansOfType.forEach((k,v)->{
            parse.put(v.getType(),v);
        });
    }

    /**
     * 初始化form.xml文件
     * @param root
     */
    private void parseForm(Element root) {
        BaseForm form = new BaseForm();
        String formName = root.getAttribute(FormConst.ATTR_NAME);
        form.setName(formName);
        form.setId(root.getAttribute(FormConst.ATTR_ID));
        //初始化fields
        preForm(root, form,FormConst.OPTIONS_MAP);
        preForm(root, form,FormConst.FIELDS);
        preForm(root, form,FormConst.COMMON_FORM);
        NodeList childNodes = root.getChildNodes();
        //初始化其他属性
        for(int i=0;i<childNodes.getLength();i++){
            Node item = childNodes.item(i);
            if(item instanceof Element){
                FormParse formParse = parse.get(item.getNodeName());
                formParse.parse(item,form);
            }
        }
        formMap.put(form.getId(),form);
        if(StringUtils.equals(form.getId(),"Common")){
            AbstractFormParse.initCommonForm(form);
        }
    }

    /**
     * 加载文件中的名称
     * @param form
     */
    public static void loadFieldI18Name(BaseForm form) {
        String name = form.getId();
        JSONObject fieldObj = i18Map.get(name);
        if(fieldObj==null){
            logger.warn("i18 load file is not fund");
            return;
        }
        String formName = form.getName();
        formName=formName.substring(formName.lastIndexOf(".")+1);
        Optional.ofNullable(fieldObj.getString(formName)).ifPresent(zhName->form.setName(zhName));
        Map<String, Field> fields = form.getFields();
        fields.forEach((key,item)->{
            String fullFieldName = item.getName();
            String fieldName = fullFieldName.substring(fullFieldName.lastIndexOf(".")+1);
            String zhName = Optional.ofNullable(fieldObj.getString(fieldName)).orElse(fieldName);
            item.setName(zhName);
        });
    }

    /**
     * 加载文件中的名称
     * @param fields
     * @return
     */
    public static List<Action> loadActionsI18Name(List<Action> fields) {
        JSONObject fieldObj = i18Map.get("Common");
        if(fieldObj==null){
            logger.warn("i18 load file is not fund");
            return fields;
        }
        fields.forEach((item)->{
            String fullFieldName = item.getName();
            String fieldName = fullFieldName.substring(fullFieldName.lastIndexOf(".")+1);
            Optional.ofNullable(fieldObj.getString(fieldName)).ifPresent(zhName->item.setName(zhName));
        });
        return fields;
    }

    private void preForm(Element root, BaseForm form,String formName) {
        NodeList fieldNodes = root.getElementsByTagName(formName);
        Node fieldNode = fieldNodes.item(0);
        if(fieldNodes.getLength()>0){
            FormParse fieldParse = parse.get(fieldNode.getNodeName());
            fieldParse.parse(fieldNode,form);
        }
    }

    private Document createDocument(File file){
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder docBuilder = factory.newDocumentBuilder();
            return docBuilder.parse(file);
        } catch (Exception e) {
            logger.error("load xml file:"+file.getName()+"failed");
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
        bootStart();
    }

    /**
     * 加载字段配置文件
     */
    private void initI18n() {

        try {
            //扫描所用工程下i18n/zh/下的文件
            Resource[] resources = resourceResolver.getResources(I18N_PATH);
            for(Resource resource:resources){
                File file = resource.getFile();
                String fileName=file.getName();
                i18Map.put(fileName.substring(0,fileName.indexOf(".")),JsonUtils.parse(file));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initForm(){
        File baseFile = null;
        try {
            Resource[] resources = resourceResolver.getResources(FORM_PATH);
            List<Resource> resourceList = initBaseCrudFrom(resources);
            for(Resource resource:resourceList){
                Document document = createDocument(resource.getFile());
                parseForm(document.getDocumentElement());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Resource> initBaseCrudFrom(Resource[] resources) {
        List<Resource> resourcesList = new ArrayList<>(Arrays.asList(resources));
        try {
            int index=0;
            for(Resource resource:resourcesList){
                if(StringUtils.contains(resource.getFile().getAbsolutePath(),BASE_FORM_PATH)){
                    break;
                }
                index++;
            }
            Resource resource = resourcesList.remove(index);
            Document document = createDocument(resource.getFile());
            parseForm(document.getDocumentElement());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resourcesList;
    }
}
