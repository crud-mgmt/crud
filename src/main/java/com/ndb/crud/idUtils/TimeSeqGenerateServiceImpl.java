package com.ndb.crud.idUtils;

import com.ndb.crud.utils.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Random;
@Service("timeSeqGenerateService")
public class TimeSeqGenerateServiceImpl implements SeqGenerateService {
	@Override
	public String generate(String prefix) {
		if(StringUtils.isEmpty(prefix)) {			
			return SeqGenerateService.super.generate(prefix);
		}
		Random random=new Random();
		
		return prefix.concat(DateUtils.formatDate(DateUtils.TIMESTAMP_PATTERN)).concat(random.nextInt(100)+"");
	}
}
