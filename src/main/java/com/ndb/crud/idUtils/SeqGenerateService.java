package com.ndb.crud.idUtils;

import java.util.UUID;

public interface SeqGenerateService {
	
	default String generate(String prefix) {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
