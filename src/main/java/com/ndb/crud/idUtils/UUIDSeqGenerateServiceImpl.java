package com.ndb.crud.idUtils;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
@Service("uuidSeqGenerateService")
public class UUIDSeqGenerateServiceImpl implements SeqGenerateService {
	@Override
	public String generate(String prefix) {
		if(StringUtils.isEmpty(prefix)) {			
			return SeqGenerateService.super.generate(prefix);
		}
		
		return prefix.concat(SeqGenerateService.super.generate(prefix));
	}
}
