package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.Action;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.model.form.Column;
import com.ndb.crud.model.form.Field;
import com.ndb.crud.service.BaseFormService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Component
public class ColumnsFormParse extends AbstractFormParse  {
    @Override
    public String getType() {
        return FormConst.COLUMNS;
    }

    @Override
    public void parse(Node node, BaseForm form) {
        Map<String, Field> fieldMap = form.getFields();
        List<Column> columns=new ArrayList<>();
        form.setColumns(columns);
        NodeList nodeList = ((Element) node).getElementsByTagName(FormConst.COLUMN);
        for(int i=0;i<nodeList.getLength();i++){
            Element item = (Element) nodeList.item(i);
            Column column = new Column();
            String fieldId = item.getAttribute(FormConst.ATTR_ID);
            Field field = fieldMap.get(fieldId);
            if(field !=null){
                BeanUtils.copyProperties(field,column);
            }
            column.setId(fieldId);
            column.setWidth(Integer.parseInt(item.getAttribute(FormConst.ATTR_WIDTH)));
            column.setTbType(item.getAttribute(FormConst.ATTR_TB_TYPE));
            if(StringUtils.equals(FormConst.ACTION,column.getTbType())){
                NodeList actionNode = item.getElementsByTagName(FormConst.ACTIONS);
                if(actionNode.getLength()>0){
                    List<Action> actions = getActions(actionNode.item(0));
                    actions= BaseFormService.loadActionsI18Name(actions);
                    column.setActions(actions);
                }
            }
            columns.add(column);
        }
    }
}
