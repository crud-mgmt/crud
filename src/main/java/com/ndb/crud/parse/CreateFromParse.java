package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.model.form.ShowForm;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
@Component
public class CreateFromParse extends AbstractFormParse  {
    @Override
    public String getType() {
        return FormConst.CREATE_FORM;
    }

    @Override
    public void parse(Node node, BaseForm form) {
        ShowForm createForm = new ShowForm();
        form.setCreateForm(getFormDetail(node,form));
    }
}
