package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.model.form.Field;
import com.ndb.crud.model.form.OptionRef;
import com.ndb.crud.service.BaseFormService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Component
public class FieldsFormParse extends AbstractFormParse  {
    @Override
    public String getType() {
        return FormConst.FIELDS;
    }

    @Override
    public void parse(Node node, BaseForm form) {
        if(!CollectionUtils.isEmpty(form.getFields())) {
            return;
        }
        Map<String,Field> fields=new HashMap<>();
        NodeList childNodes = node.getChildNodes();
        for (int i=0;i<childNodes.getLength();i++){
            Node childNode = childNodes.item(i);
            if(!(childNode instanceof Element)){
                continue;
            }
            Element item= (Element) childNode;
            Field field=new Field();
            field.setId(item.getAttribute(FormConst.ATTR_ID));
            field.setType(item.getAttribute(FormConst.ATTR_TYPE));
            field.setName(item.getAttribute(FormConst.ATTR_NAME));
            field.setRequired(Boolean.valueOf(item.getAttribute(FormConst.ATTR_REQUIRED)));
            if(StringUtils.equals(field.getType(),FormConst.ATTR_SELECT)){
                String optionRefId = item.getAttribute(FormConst.ATTR_OPTION_REF);
                Map<String, List<OptionRef>> optionRefs = form.getOptionRefs();
                if(!CollectionUtils.isEmpty(optionRefs)){
                    field.setOptionRef(optionRefs.get(optionRefId));
                }
            }
            NodeList fieldNodes = item.getChildNodes();
            for (int j=0;j<fieldNodes.getLength();j++){
                Node attrNode = fieldNodes.item(j);
                if(!(attrNode instanceof Element)){
                    continue;
                }
                if(StringUtils.equals(FormConst.REGEXP,((Element)attrNode).getTagName())){
                    field.setRegexp(attrNode.getTextContent());
                }
                if(StringUtils.equals(FormConst.FORMAT,((Element)attrNode).getTagName())){
                    field.setFormat(attrNode.getTextContent());
                }
            }
            fields.put(field.getId(),field);
        }
        form.setFields(fields);
        BaseFormService.loadFieldI18Name(form);
    }

}
