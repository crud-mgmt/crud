package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.Action;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.service.BaseFormService;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;

import java.util.List;
@Component
public class ActionsFormParse extends AbstractFormParse {
    @Override
    public String getType() {
        return FormConst.ACTIONS;
    }

    @Override
    public void parse(Node node, BaseForm form) {
        List<Action> actions=getActions(node);
        actions=BaseFormService.loadActionsI18Name(actions);
        form.setActions(actions);
    }
}
