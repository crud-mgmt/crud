package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.*;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractFormParse implements FormParse{
    private static BaseForm commonForm;
    protected ShowForm getIncludeFrom(Node node, BaseForm form){
        String refId = ((Element)node).getAttribute(FormConst.ATTR_ID);
        Map<String, ShowForm> commonForms = form.getCommonForms();
        return commonForms.get(refId);
    }

    public static void initCommonForm(BaseForm form){
        commonForm=form;
    }

    public BaseForm getCommonForm(){
        return commonForm;
    }

    protected List<Action> getActions(Node node) {
        List<Action> actions=new ArrayList<>();
        NodeList childNodes = node.getChildNodes();
        for(int i=0;i<childNodes.getLength();i++){
            Node childNode = childNodes.item(i);
            if(childNode instanceof Element){
                Element item = (Element)childNode;
                String ref = item.getAttribute(FormConst.ATTR_REF);
                Optional<Action> actionRef = getActionRef(ref);
                Action action=null;
                if(actionRef.isPresent()){
                    action = actionRef.get();
                }else {
                    action=new Action();
                    action.setId(item.getAttribute(FormConst.ATTR_ID));
                    action.setName(item.getAttribute(FormConst.ATTR_NAME));
                    action.setIcon(item.getAttribute(FormConst.ATTR_ICON));
                }
                //支持引用优先级
                Optional.ofNullable(item.getFirstChild()).map(Node::getNodeValue).ifPresent(action::setPath);
                if(StringUtils.isNotBlank(item.getAttribute(FormConst.ATTR_AC_TYPE))){
                    action.setAcType(item.getAttribute(FormConst.ATTR_AC_TYPE));
                }
                actions.add(action);
            }
        }
        return actions;
    }

    protected ShowForm getFormDetail(Node node, BaseForm form){
        ShowForm showForm=new ShowForm();
        List<Row> rows=new ArrayList<>();
        NodeList childNodes = node.getChildNodes();
        for(int i=0;i<childNodes.getLength();i++){
            Node item = childNodes.item(i);
            if(item instanceof Element){
                Row row = new Row();
                if(StringUtils.equals(((Element)item).getTagName(),FormConst.INCLUDE_FROM)){
                    getIncludeFrom(item,form);
                }
                List<Field> cols = getColField((Element)item,form);
                row.setCols(cols);
                rows.add(row);
            }
        }
        showForm.setRows(rows);
        return showForm;
    }

    protected List<Field> getColField(Element item, BaseForm form) {
        List<Field> cols=new ArrayList<>();
        Map<String, Field> fieldMap = form.getFields();
        NodeList colItem = item.getElementsByTagName(FormConst.COL);
        for(int i=0;i<colItem.getLength();i++){
            Element item1 = (Element) colItem.item(i);
            String fieldId = item1.getAttribute(FormConst.ATTR_ID);
            Field field = fieldMap.get(fieldId);
            if(field!=null){
                cols.add(field);
            }
        }
        return cols;
    }

    /**
     * 根据ref获取common的action
     * @param ref
     * @return
     */
    protected Optional<Action> getActionRef(String ref){
        if(StringUtils.isNotBlank(ref)){
            BaseForm commonForm = getCommonForm();
            List<Action> actionList = commonForm.getActions();
            return actionList.stream().filter(actionItem->StringUtils.equals(actionItem.getId(),ref)).findFirst();
        }
        return Optional.empty();
    }

    protected List<OptionRef> getOptionRef(String ref){
        if(StringUtils.isBlank(ref)){
            return new ArrayList<>();
        }
        BaseForm commonForm = getCommonForm();
        Map<String, List<OptionRef>> optionRefs = commonForm.getOptionRefs();
        return optionRefs.get(ref);
    }
}
