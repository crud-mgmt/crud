package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.model.form.Field;
import com.ndb.crud.model.form.Filter;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class FiltersFormParse extends AbstractFormParse  {
    @Override
    public String getType() {
        return FormConst.FILTERS;
    }

    @Override
    public void parse(Node node, BaseForm form) {
        List<Filter> filters=new ArrayList<>();
        Map<String, Field> fieldMap = form.getFields();
        NodeList childNodes = node.getChildNodes();
        for (int i=0;i<childNodes.getLength();i++){
            Node childNode = childNodes.item(i);
            if(childNode instanceof Element){
                Element item=(Element) childNode;
                Filter filter = new Filter(item.getAttribute(FormConst.ATTR_ID), item.getAttribute(FormConst.OPTION));
                Field field = fieldMap.get(filter.getId());
                if(field!=null){
                    BeanUtils.copyProperties(field,filter);
                }
                filters.add(filter);
            }
        }

        form.setFilters(filters);
    }

}
