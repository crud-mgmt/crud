package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.model.form.ShowForm;
import org.springframework.stereotype.Component;
import org.w3c.dom.Node;
@Component
public class UpdateFormParse extends AbstractFormParse  {
    @Override
    public String getType() {
        return FormConst.UPDATE_FORM;
    }

    @Override
    public void parse(Node node, BaseForm form) {
        ShowForm createForm = new ShowForm();
        form.setUpdateForm(getFormDetail(node,form));
    }
}
