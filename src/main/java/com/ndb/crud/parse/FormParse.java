package com.ndb.crud.parse;

import com.ndb.crud.model.form.BaseForm;
import org.w3c.dom.Node;

public interface FormParse {
    public String getType();

    public void parse(Node node, BaseForm form);
}
