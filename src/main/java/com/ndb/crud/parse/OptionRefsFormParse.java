package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.model.form.OptionRef;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class OptionRefsFormParse extends AbstractFormParse  {
    @Override
    public String getType() {
        return FormConst.OPTIONS_MAP;
    }

    @Override
    public void parse(Node node, BaseForm form) {
        Map<String, List<OptionRef>> optionRefs = form.getOptionRefs();
        if(!CollectionUtils.isEmpty(optionRefs)){
            return;
        }
        optionRefs=new HashMap<>();
        form.setOptionRefs(optionRefs);
        NodeList childNodes = node.getChildNodes();
        //optionRef列表
        for(int i=0;i<childNodes.getLength();i++){
            Node childNode = childNodes.item(i);
            if(childNode instanceof Element){
                Element item= (Element) childNode;
                String ref = item.getAttribute(FormConst.ATTR_REF);
                List<OptionRef> optionRef = getOptionRef(ref);
                if(CollectionUtils.isEmpty(optionRef)){
                    optionRefs.put(item.getAttribute(FormConst.ATTR_ID),getOptions(item));
                }else {
                    optionRefs.put(item.getAttribute(FormConst.ATTR_ID),optionRef);
                }
            }
        }
    }

    /**
     * 获取option列表
     * @param item
     * @return
     */
    private List<OptionRef> getOptions(Element item) {
        List<OptionRef> list = new ArrayList<>();
        NodeList optionNodes = item.getChildNodes();
        for(int n=0;n<optionNodes.getLength();n++){
            Node optionNode = optionNodes.item(n);
            if(optionNode instanceof Element){
                Element optionElement=(Element)optionNode;
                list.add(new OptionRef(optionElement.getAttribute(FormConst.ATTR_KEY),optionElement.getTextContent()));
            }
        }
        return list;
    }
}
