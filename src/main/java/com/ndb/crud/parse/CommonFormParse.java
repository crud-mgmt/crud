package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.model.form.ShowForm;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.HashMap;
import java.util.Map;
@Component
public class CommonFormParse extends AbstractFormParse  {
    @Override
    public String getType() {
        return FormConst.COMMON_FORM;
    }

    @Override
    public void parse(Node node, BaseForm form) {
        Map<String, ShowForm> commonForms1 = form.getCommonForms();
        if(!CollectionUtils.isEmpty(commonForms1)){
            return;
        }
        ShowForm showForm = getFormDetail(node, form);
        Map<String, ShowForm> commonForms=new HashMap<>();
        commonForms.put(((Element)node).getAttribute(FormConst.ATTR_ID),showForm);
        form.setCommonForms(commonForms);
    }
}
