package com.ndb.crud.parse;

import com.ndb.crud.common.constants.FormConst;
import com.ndb.crud.model.form.BaseForm;
import com.ndb.crud.model.form.Orders;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
@Component
public class OrdersFormParse extends AbstractFormParse  {
    @Override
    public String getType() {
        return FormConst.ORDERS;
    }

    @Override
    public void parse(Node node, BaseForm form) {
        Orders orders=new Orders();
        orders.setSort(((Element)node).getAttribute(FormConst.ATTR_SORT));
        form.setOrders(orders);
        List<String> lists=new ArrayList<>();
        NodeList childNodes = node.getChildNodes();
        for(int i=0;i<childNodes.getLength();i++){
            Node item = childNodes.item(i);
            if(item instanceof Element){
                lists.add(((Element)node).getAttribute(FormConst.ATTR_ID));
            }
        }
        orders.setOrders(lists);
    }
}
