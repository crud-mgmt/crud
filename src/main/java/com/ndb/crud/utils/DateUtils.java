package com.ndb.crud.utils;

import com.ndb.parent.common.constant.CommonMsg;
import com.ndb.parent.common.exception.NdbException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	//y-代表年,M-代表月,d-代表日,H-代表24进制的小时,h-代表12进制的小时
    //m-代表分钟,s-代表秒,S-代表毫秒
	
	/**yyyy-MM-dd HH:mm:ss.SSS */
	public static final String TIMESTAMP_NORMAL_PATTERN="yyyy-MM-dd HH:mm:ss.SSS";
	
	/**yyyyMMddHHmmssSSS */
	public static final String TIMESTAMP_PATTERN="yyyyMMddHHmmssSSS";
	
	/**yyyy-MM-dd HH:mm:ss */
	public static final String DATE_TIME_NORMAL_PATTERN="yyyy-MM-dd HH:mm:ss";
	
	/**yyyyMMddHHmmss */
	public static final String DATE_TIME_PATTERN="yyyyMMddHHmmss";
	
	/**yyyy-MM-dd */
	public static final String DATE_NORMAL_PATTERN="yyyy-MM-dd";
	
	/**yyyyMMdd */
	public static final String DATE_PATTERN="yyyyMMdd";
	
	
	public static String currentDate() {
		return formatDate(DATE_TIME_PATTERN);
	}
	
	public static String formatDate(String pattern) {
		Date date = new Date();
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	public static String formatDate(Date date,String pattern) {
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	public static Date parseDate(String date,String pattern) {
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			throw new NdbException(CommonMsg.DATE_FORMAT_FAILED);
		}
	}
}
