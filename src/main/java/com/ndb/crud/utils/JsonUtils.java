package com.ndb.crud.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.IOUtils;

import java.io.*;

public class JsonUtils {
    public static JSONObject parse(File file){
        if(!file.exists()){
            throw new RuntimeException("file not found");
        }
        try(
            FileInputStream inputStream = new FileInputStream(file);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ) {
            IOUtils.copy(inputStream,outputStream);
            JSONObject object = JSONObject.parseObject(outputStream.toByteArray(), JSONObject.class);
            return object;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("file not found");
        }
    }
}
