package com.ndb.crud.aspect;

import com.ndb.crud.annotation.FieldAuto;
import com.ndb.crud.common.constants.FieldAutoType;
import com.ndb.crud.context.MgmtContext;
import com.ndb.crud.utils.InvokeUtils;
import com.ndb.parent.common.model.UserInfo;
import com.ndb.crud.idUtils.SeqGenerateService;
import com.ndb.crud.idUtils.TimeSeqGenerateServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

@Aspect
@Component
public class FieldAutoAspect {
    Logger logger= LoggerFactory.getLogger(FieldAutoAspect.class);

    private SeqGenerateService seqGenerateService=new TimeSeqGenerateServiceImpl();

    @Pointcut("@annotation(com.ndb.crud.annotation.MethodFill)")
    public void pc(){}

    @Before(value = "pc()")
    public void before(JoinPoint point){
        Object[] params = point.getArgs();
        if(params.length==0){
            return;
        }
        Object param= params[0];
        Class<?> paramCls = param.getClass();
        List<Field> fields = InvokeUtils.getFieldsAll(paramCls);
        for(Field field :fields){
            boolean flag = field.isAnnotationPresent(FieldAuto.class);
            if(!flag){
                continue;
            }
            FieldAuto fieldAuto = field.getAnnotation(FieldAuto.class);
            String importType = fieldAuto.importType();
            String value = fieldAuto.value();
            switch (importType){
                case FieldAutoType.ID:
                    initFieldId(param,field,fieldAuto);
                    break;
                case FieldAutoType.DATE:
                    initFieldDate(param,field,fieldAuto);
                    break;
                case FieldAutoType.OPERATION:
                    initFieldOpt(param,field,fieldAuto);
                    break;
            }
        }
    }

    private void initFieldOpt(Object obj, Field field, FieldAuto fieldAuto) {
        UserInfo user = MgmtContext.getContext().getUser();
        if(user!=null){
            try {
                field.setAccessible(true);
                field.set(obj,user.getName());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                logger.error("field auto set failed",e.getMessage());
            }
        }

    }

    private void initFieldDate(Object obj, Field field, FieldAuto fieldAuto) {
        try {
            Date date = new Date();
            field.setAccessible(true);
            field.set(obj,date);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            logger.error("field auto set failed",e.getMessage());
        }
    }

    /**
     * ID自动赋值
     * @param obj
     * @param field
     * @param fieldAuto
     */
    private void initFieldId(Object obj, Field field, FieldAuto fieldAuto) {
        try {
            String prefix = fieldAuto.value();
            if(StringUtils.isBlank(prefix)){
                logger.warn("id生成器表前缀prefix为空");
            }
            String id = seqGenerateService.generate(prefix);
            field.setAccessible(true);
            field.set(obj,id);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            logger.error("field auto set failed",e.getMessage());
        }
    }
}
