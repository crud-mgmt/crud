package com.ndb.crud.annotation;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD}) //注解应用类型
@Retention(RetentionPolicy.RUNTIME) // 注解的类型
public @interface FieldAuto {
    String value() default "";
    //字段填充类型
    String importType() default "";
}
