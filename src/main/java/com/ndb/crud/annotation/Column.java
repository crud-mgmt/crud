package com.ndb.crud.annotation;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD}) //注解应用类型
@Retention(RetentionPolicy.RUNTIME) // 注解的类型
public @interface Column {
    /**
     * 如果注解有属性则取属性的值，否则按照头峰标识进行解析成数据库对应字段
     * 例如：userName解析成数据库字段则为user_name
     * @return
     */
    String dbFieldName() default "";

    /**
     * 是否是主键，查询是根据主键判断重复
     * @return
     */
    boolean identity() default false;
}
