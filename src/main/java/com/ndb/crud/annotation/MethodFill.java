package com.ndb.crud.annotation;

import java.lang.annotation.*;

@Documented
@Target({ElementType.METHOD}) //注解应用类型
@Retention(RetentionPolicy.RUNTIME) // 注解的类型
public @interface MethodFill {
}
