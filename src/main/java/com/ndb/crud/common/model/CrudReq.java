package com.ndb.crud.common.model;

import com.ndb.crud.model.CrudValid;
import com.ndb.parent.common.model.Req;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;
@Getter
@Setter
public class CrudReq<Model,TID> extends Req {
    @NotNull(groups ={CrudValid.QueryValid.class} )
    private TID key;

    @NotNull(groups ={CrudValid.UpdateValid.class} )
    private Model data;

    private List<Model> list;
}
