package com.ndb.crud.common.model;

import com.ndb.crud.model.ConditionFields;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

public class CommonQueryReq extends PageReq {
    private List<ConditionFields> fields;

    @NotEmpty
    private List<String> orderList;

    /**排序方式 asc,desc*/
    @NotBlank
    private String orderCase;

    public List<ConditionFields> getFields() {
        return fields;
    }

    public void setFields(List<ConditionFields> fields) {
        this.fields = fields;
    }

    public List<String> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<String> orderList) {
        this.orderList = orderList;
    }

    public String getOrderCase() {
        return orderCase;
    }

    public void setOrderCase(String orderCase) {
        this.orderCase = orderCase;
    }
}
