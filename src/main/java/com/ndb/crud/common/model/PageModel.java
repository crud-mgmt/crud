package com.ndb.crud.common.model;

import com.ndb.parent.common.model.Res;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class PageModel <Module> extends Res{
    private long total;

    private List<Module> list;

    public PageModel(List<Module> list){
        this.list=list;
    }
}
