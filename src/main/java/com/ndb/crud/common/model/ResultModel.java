package com.ndb.crud.common.model;

import com.ndb.parent.common.model.Res;

public class ResultModel<Model>extends Res {
    private Model data;
    public static ResultModel ok(Object data){
        ResultModel model=new ResultModel();
        model.data=data;
        return model;
    }

    public static ResultModel ok(){
        ResultModel model=new ResultModel();
        return model;
    }
}
