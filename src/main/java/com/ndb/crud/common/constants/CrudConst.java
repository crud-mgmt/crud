package com.ndb.crud.common.constants;

public class CrudConst {
    public static class Status{
        public static String ACTIVE="N";
        public static String LOCK="L";
        public static String DEL="D";
    }
}
