package com.ndb.crud.common.constants;

/**
 * FieldAuto填充类型
 */
public class FieldAutoType {
    /**
     * ID填充
     */
    public static final String ID="id";
    /**
     * 日期填充
     */
    public static final String DATE="date";
    /**
     * 操作者填充
     */
    public static final String OPERATION="operation";
}
