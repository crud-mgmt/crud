package com.ndb.crud.common.constants;

public class FormConst {
    public static final String FORM="form";
    public static final String FILTERS="filters";
    public static final String FILTER="filter";
    public static final String ORDERS="orders";
    public static final String ORDER="order";
    public static final String REGEXP="regexp";
    public static final String FORMAT="format";
    public static final String FIELDS="fields";
    public static final String FIELD="field";
    public static final String ACTIONS="actions";
    public static final String ACTION="action";
    public static final String PATH="path";
    public static final String CREATE_FORM="createForm";
    public static final String ROW="row";
    public static final String COL="col";
    public static final String UPDATE_FORM="updateForm";
    public static final String INCLUDE_FROM="includeFrom";
    public static final String DETAIL_FORM="detailForm";
    public static final String CRUD_FORM="crudForm";
    public static final String COMMON_FORM="commonForm";
    public static final String COLUMNS="columns";
    public static final String COLUMN="column";
    public static final String OPTIONS_MAP="optionsMap";
    public static final String OPTION_REF="optionRef";
    public static final String OPTION="option";

    public static final String ATTR_ID="id";
    public static final String ATTR_OPTION="option";
    public static final String ATTR_SORT="sort";
    public static final String ATTR_TYPE="type";
    public static final String ATTR_NAME="name";
    public static final String ATTR_AC_TYPE="acType";
    public static final String ATTR_TB_TYPE="tbType";
    public static final String ATTR_KEY="key";
    public static final String ATTR_WIDTH="width";
    public static final String ATTR_OPTION_REF="optionRef";
    public static final String ATTR_REF="ref";
    public static final String ATTR_SELECT="select";
    public static final String ATTR_REQUIRED="required";
    public static final String ATTR_ICON="icon";
}
