package com.ndb.crud.common.constants;

public enum QueryOptionEnum {
    EQ("="),
    NOT_EQ("<>"),
    LIKE("like"),
    IN("in"),
    GT(">"),
    GT_EQ(">="),
    LT("<"),
    LT_EQ("<="),
    BETWEEN("between")
    ;
    private String name;

    QueryOptionEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
