package com.ndb.crud.dao;

import com.ndb.crud.annotation.MethodFill;
import com.ndb.crud.common.constants.CrudConst;
import com.ndb.crud.model.CommonQuery;
import org.apache.ibatis.annotations.Param;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.List;

public interface CrudDao <Module,TID>{
    /**
     * 分页查询
     * @param condition
     * @return
     */
    public List<Module> query(CommonQuery condition);

    /**
     * 查询详情
     * @param condition
     * @return
     */
    public default Module detail(CommonQuery condition){
        List<Module> result = query(condition);
        return CollectionUtils.isEmpty(result)?null:result.get(0);
    }

    /**
     * 查询详情
     * @param id
     * @return
     */
    public Module detail(TID id);

    /**
     * 批量插入数据
     * @param data
     * @return
     */
    public boolean batchInsert(List<Module> data);

    /**
     * 插入数据
     * @param model
     * @return
     */
    @MethodFill
    public boolean insert(Module model);

    /**
     * 更新数据
     * @param model
     * @param keyId 主键Id
     * @return
     */
    @MethodFill
    public boolean update(@Param("model") Module model,@Param("seq") TID keyId);

    /**
     * 假删除
     * @param id
     * @return
     */
    public boolean delete(TID id);

    /**
     * 假删除,如果有状态status=D
     * @param id
     * @return
     */
    public default boolean deleteMark(Class<?> cls,TID id){
        try {
            Field field = cls.getDeclaredField("status");
            if(field!=null){
                Module instance = (Module) cls.newInstance();
                field.setAccessible(true);
                field.set(instance, CrudConst.Status.DEL);
                return update(instance,id);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
