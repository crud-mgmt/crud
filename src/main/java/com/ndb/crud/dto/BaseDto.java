package com.ndb.crud.dto;

import com.ndb.crud.annotation.FieldAuto;
import com.ndb.crud.common.constants.FieldAutoType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class BaseDto implements Serializable {
    @FieldAuto(importType = FieldAutoType.ID)
    private String seq;

    @FieldAuto(importType = FieldAutoType.OPERATION)
    private String createUser;

    @FieldAuto(importType = FieldAutoType.DATE)
    private Date createTime;

    @FieldAuto(importType = FieldAutoType.OPERATION)
    private String updateUser;

    @FieldAuto(importType = FieldAutoType.DATE)
    private Date updateTime;
}
