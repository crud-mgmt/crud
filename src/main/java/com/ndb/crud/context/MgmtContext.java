package com.ndb.crud.context;

import com.ndb.parent.common.model.UserInfo;

public class MgmtContext {
    private UserInfo userInfo;
    private static MgmtContext context=new MgmtContext();

    public static MgmtContext getContext(){
        return context;
    }

    public MgmtContext setUserInfo(UserInfo userInfo){
        context.userInfo=userInfo;
        return context;
    }

    public UserInfo getUser(){
        return context.userInfo;
    }
}
