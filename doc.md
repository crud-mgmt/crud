快速接入：简单的增删查改前后端可配置框架，致力于快速生成前后端增、删、查、改、功能，让你不再关心crud，前端页面
* 第一步
```
1、安装EasyCode-MybatisCodeHelper插件
2、将resource/easycodeTemplate/目录下的配置导入到插件中
3、连接数据库信息
4、展开打开数据库
菜单选择：选择表-easyCodeMybatisCodeHelper-Generate Code
勾选：entity，dao,service,serviceImpl,form,formName自动生成
5、优化form.xml前段配置展示信息
6、注入BaseFormService，调用注入BaseFormService.meta(formName)查询前段表单配置信息
```

* service层：
```java
//接口定义
public interface MenuRoleService extends CrudService<MenuRole,String>{
}
//接口实现类
@Service
public class MenuRoleServiceImpl extends BaseCurdService<MenuRole,String, MenuRoleDao> implements MenuRoleService {
}
```
* 表中文字段存放目录：
```
路径：resource/i18n/zh/
文件命名规则：tableName.json
```
* 前端配置文件
```
路径：resource/i18n
文件命名规则：tableNameForm.xml
```