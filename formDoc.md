###前端表单展示信息维护说明
####标签解析
####1、form
每个菜单对应这一个表单，所有标签的根标签，一个文件只能对应一个form标签
#####2、filters-查询条件组
子标签：filter-查询条件对应的字段
属性
- id:查询条件的唯一标识，对应这field标签的ID
- option：查询类型：eq-全值匹配，like-模糊匹配，between-介于两者之间
#####3、orders 表格排序组
属性
- sort：排序类型：asc-升序，desc-降序
子标签：order-排序字段配置，属性id-对应者field标签的id
#####4、actions 操作按钮组
子标签：action
属性：
- id：按钮的唯一标识
- name:前端展示的按钮中文名
- acType：点击按钮后的动作类型，dialog-弹框形式，page-跳转新页面
- icon：按钮图标
- ref：引用的按钮id
- path：acType为page跳转的页面路径
#####4、fields 当前表单展示的所有基础字段
子标签：field
属性：
- id：数据库表字段(头峰命名格式)
- name:字段中文名
- type：字段类型，select-下拉选择框，input-文本，radio-单选框，id-表的主键标识
- optionRef：需要翻译的字典引用信息
- required：当前字段是否必填(表单提交时校验)
- regexp：正则匹配规则，表单提交时校验,TODO
- format：字段展示格式化,TODO
#####5、crudForm 新增，修改，详情页面的表单配置
#####6、createForm 新增页面的表单配置
#####7、updateForm 修改页面的表单配置
#####8、detailForm 详情页面的表单配置
#####9、页面表单配置子标签
- row：行信息
- col：列信息，属性id，field标签的id
- includeFrom：表单页面引用的公共字段信息，id对应commonForm的id
#####10、commonForm 公共字段配置
#####11、columns表格展示的列配置
子标签：column
属性：
- id:展示的字段
- width：列的宽度
- tbType：当前列属性：action-操作列，text-文本
- 子标签：actions，当列属性为action时配置按钮信息
#####12、optionsMap数据字典映射
子标签：optionRef-数据字典配置：
属性：
- id：字典唯一值，field标签optionRef属性引用
- ref：引用的公共数据字典的id
子标签：option
```
key:字典的key，如：
<option key="0">否</option>
```
